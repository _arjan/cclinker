# cclinker

Use another c linker than the Clean optimized linker.

Compile:
```bash
export CLEAN_HOME=/path/to/clean
$CLEAN_HOME/bin/clm -nr -nt -IL Platform -IL Dynamics -IL Generics cclinker -o cclinker
```
